import SidebarItemList from 'src/core/components/sidebar/SidebarItemList';
// import SelectionDialog from 'src/core/components/selectionDialog/selectionDialog';

export default angular
    .module('core', ['ngMaterial'])
    .component(SidebarItemList.name, SidebarItemList.config);
    // .component(SelectionDialog.name, SelectionDialog.config);
    // .service("GetLDResourceService", GetLDResourceService);
    // .factory("GetLDResourceService", GetLDResourceService);
