// import rdflib from 'src/core/rdflib/firstrdflib.min';
import rdflib from 'src/core/rdflib/rdflib';

// Namespace definitions
var RDF = rdflib.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
var OSLC = rdflib.Namespace("http://open-services.net/ns/core#");
var BEX = rdflib.Namespace("http://basic.example/nsp#");
var DCTERMS = rdflib.Namespace("http://purl.org/dc/terms/#");
var RDFS = rdflib.Namespace("http://www.w3.org/2000/01/rdf-schema#");

// https://gist.github.com/ckristo/9121f31913d21d62c951

export function getResource ($http, request_full_url, callback) {
    console.log('getResource called, url: ' + request_full_url);
    $http({
        method: 'GET',
        url: request_full_url,
        headers: {
            'Accept': 'application/rdf+xml'
        },
    }).then(function successCallback(response) {
        var kb = new rdflib.IndexedFormula();
        rdflib.parse(response.data, kb, request_full_url, 'application/rdf+xml');

        var jobj = new LDResource(request_full_url);
        fromRDF(kb, kb.sym(request_full_url), jobj);
        jobj.rdfgraph = kb;
        // console.log(jobj);

        // rdflib.serialize(undefined, kb, undefined, 'application/rdf+xml', function(err, str) {
        //     console.log("serialize xml");
        //     console.log(str);
        //     console.log(err);
        // });

        callback(jobj);

    }, function errorCallback(response) {
        alert('getResource failed');
    });

};

export function updateResource ($http, resource_full_url, rdfgraph, callback) {
    console.log('updateResource called, url: ' + resource_full_url);

    var data;
    rdflib.serialize(undefined, rdfgraph, undefined, 'application/rdf+xml', function(err, str){

        $http({
            method: 'PUT',
            url: resource_full_url,
            data: str,
            headers: {
                'Accept': 'application/rdf+xml',
                'Content-Type': 'application/rdf+xml'
            },
        }).then(function successCallback(response) {

            callback("OK");

        }, function errorCallback(response) {
            alert('updateResource failed');
        });

    });

};

export function updateRDFGraphWithLinks (sourceURI, sourceRdfGraph, linkProp, targetURLs) {
    for (var x in targetURLs) {
        // TODO: automatically infer the proper name graph
        // console.log(sourceURI);
        // console.log(targetURLs[x].identifier);
        var node = sourceRdfGraph.sym(sourceURI);
        // console.log(node);
        sourceRdfGraph.add(node, BEX('develops'), sourceRdfGraph.sym(targetURLs[x].identifier));
    }
};


// Linked Data Resource
function LDResource(about) {
    this.about = about;
}

/** Introspect an RDF object's properties and values, and put them
 * into the JavaScript object
 * (extended from OSLC client javascript lib)
 *
 * @param kb - the rdflib IndexedFormula that contains the RDF graph
 * @param subject - the RDF object (an rdflib sym)
 * @param {Object} jObject - a JavaScript object that will get the discovered properties
 * @returns {Boolean} true if the properties were filled in, false if the subject is an external reference
 */
export function fromRDF (kb, subject, jObject) {
    var props = kb.statementsMatching(subject, undefined, undefined);
    if (props.length === 0) return false;
    for (var p in props) {
        var prop = props[p].predicate.uri.replace(/.*(#|\/)/, '');
        var propType = prop + '.type';
        var multiValued = false;
        if (jObject[prop] !== undefined) {
            if (!Array.isArray(jObject[prop])) {
                jObject[prop] = [jObject[prop]];
            }
            multiValued = true;
        }
        var value = null;
        if (props[p].object.termType === 'Literal') {
            // FIXME: Why rdflib does not handle properly values for literal defined as XMLLiteral from the server side?
            var valueToCopy;
            if (props[p].object.value.startsWith("<dcterms:title rdf:parseType=")) {
                var i1 = props[p].object.value.indexOf("\">");
                var i2 = props[p].object.value.indexOf("<\/");
                valueToCopy = props[p].object.value.substring(i1 + 2, i2);
            } else {
                valueToCopy = props[p].object.value;
            }
            // value = props[p].object.value;
            value = valueToCopy
        } else {
            value = {};
            if (!fromRDF(kb, props[p].object, value)) value = props[p].object.uri;
        }
        if (multiValued) {
            jObject[prop].push(value);
        } else {
            jObject[prop] = value;
        }
        jObject[propType] = props[p].object.termType;
    }
    return true;
}

// Old methods
export function expandNSPrefixes(jsonObj, prefixes){
    for(var key in prefixes){
        replacePrefix(jsonObj, key, prefixes[key]);
    }
}

function replacePrefix(jsonObj, prefix, fullNS) {
    for (var key in jsonObj) {
        if(Array.isArray(jsonObj[key])) {
            for (var key2 in jsonObj[key]) {
                replacePrefix(jsonObj[key][key2], prefix, fullNS);
            }
        }
        var str = key.substr(0, key.indexOf(':'));
        if (str === prefix) {
            var newKey = key.substr(prefix.length + 1);
            newKey = fullNS + '/' + newKey;
            jsonObj[newKey] = jsonObj[key];
            delete jsonObj[key];
        }
    }
}

export function deleteNSPrefixes(jsonObj) {
    for (var key in jsonObj) {
        if(Array.isArray(jsonObj[key])) {
            for (var key2 in jsonObj[key]) {
                deleteNSPrefixes(jsonObj[key][key2]);
            }
        }
        var newKey = key.substr(key.indexOf(':') + 1);
        jsonObj[newKey] = jsonObj[key];
        delete jsonObj[key];
    }
}
