
export default {
  name : 'sidebarItemList',
  config : {
    bindings         : {  items: '<', selected: '<', showDetails : '&onSelected' },
    templateUrl      : 'src/core/components/sidebar/SidebarItemList.html'
  }
};
