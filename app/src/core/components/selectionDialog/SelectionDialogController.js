import AppConstants from 'src/views/AppConstants';
import {getResource} from 'src/core/AppUtils';
import {AppConfig, stringifiedEndPoints, getURL} from 'src/config/AppConfig';

// function SelectionDialogController($scope, $mdDialog, $resource, $http, endpoints) {
function SelectionDialogController($scope, $mdDialog, $resource, $http) {
    console.log("SelectionDialogController called");

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.expand = function () {
        $mdDialog.fullscreen();
    }

    $scope.answer = function(ev) {
        for (var x in $scope.queryresult) {
            if ($scope.queryresult[x].selected) {
                $scope.selectedEntries.push($scope.queryresult[x].about);
            }
        }
        // console.log($scope.selectedEntries);
        $mdDialog.hide($scope.selectedEntries);
    };

    // Variables used in the HTML template
    // $scope.endpoints = endpoints;
    $scope.endpoints = AppConfig.endpoints;
    $scope.inputs = stringifiedEndPoints ($scope.endpoints);
    $scope.queryresult;
    // $scope.selectedInput;
    $scope.selectedEntries = [];
    $scope.isLoading = false;

    $scope.entryValueToDisplay = function entryValueToDisplay (entry) {
        return entry[$scope.propertyToDisplay];
    }

    $scope.triggerSearch = function triggerSearch (ev) {
        if (!$scope.selectedInput) {
            alert ("A provider must be selected");
            return;
        }
        var provider = $scope.selectedInput.split(' / ');

        $scope.isLoading = true;
        // $scope.secondLineOptions = [];
        $scope.propertyToDisplay = "title";

        var url = getURL($scope.endpoints, provider);
        // TODO: add $scope.queryfilter as a ?where parameter
        getResource($http, url, function (response) {
            $scope.queryresult = response[AppConstants.memberKey];
            $scope.isLoading = false;
            // for (var key in $scope.queryresult[0]) {
            //     if (!key.endsWith(".type")) {
            //         $scope.secondLineOptions.push(key);
            //     }
            //     $scope.secondLineOption = "type";
            // }
        });
    };

    // $scope.getSecondLineValue = function getSecondLineValue () {
    //     return "kjkj";
    // }

    $scope.getResourceInfo = function getResourceInfo (entry) {
        alert ("More info about " + entry.about);
    }
}

export default SelectionDialogController;

