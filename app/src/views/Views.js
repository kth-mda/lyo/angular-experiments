
// import UsersDataService from 'src/users/services/UsersDataService';

import Requirement from 'src/views/components/requirement/Requirement';
import Automation from 'src/views/components/automation/Automation';
import LinkCreation from 'src/views/components/linkcreation/LinkCreation';
import Quality from 'src/views/components/quality/Quality';
import Search from 'src/views/components/search/Search';
import Dashboard from 'src/views/components/dashboard/Dashboard';

export default angular
    .module("views", ['ngMaterial', 'ngResource'])

    .component(Requirement.name, Requirement.config)
    .component(Automation.name, Automation.config)
    .component(LinkCreation.name, LinkCreation.config)
    .component(Quality.name, Quality.config)
    .component(Search.name, Search.config)
    .component(Dashboard.name, Dashboard.config)

// .service("UsersDataService", UsersDataService);
