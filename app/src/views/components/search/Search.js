
import SearchController from 'src/views/components/search/SearchController';

export default {
  name : 'search',
  config : {
    templateUrl      : 'src/views/components/search/Search.html',
    controller       : [ '$resource', SearchController ]
  }
};

