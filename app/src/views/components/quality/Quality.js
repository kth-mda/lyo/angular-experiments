
import QualityController from 'src/views/components/quality/QualityController';

export default {
  name : 'quality',
  config : {
    templateUrl      : 'src/views/components/quality/Quality.html',
    controller       : [ '$resource', QualityController ]
  }
};

