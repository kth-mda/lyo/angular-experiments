
import DashboardController from 'src/views/components/dashboard/DashboardController';

export default {
  name : 'dashboard',
  config : {
    bindings         : {  items: '<', selectiondialog: '&', selectedEntries: '&'},
    templateUrl      : 'src/views/components/dashboard/Dashboard.html',
    controller       : [ DashboardController ]
  }
};

