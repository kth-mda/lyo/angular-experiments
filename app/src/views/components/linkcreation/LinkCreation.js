
import LinkCreationController from 'src/views/components/linkcreation/LinkCreationController';
// import UsersDataService from 'src/core/services/UsersDataService';
import AppConfig from 'src/config/AppConfig';

export default {
  name : 'linkCreation',
  config : {
    bindings         : {  items: '<' },
    templateUrl      : 'src/views/components/linkcreation/LinkCreation.html',
    controller       : [ '$scope', '$http', LinkCreationController ]
  }
};

