import AppConstants from 'src/views/AppConstants';
import {getResource, updateResource, updateRDFGraphWithLinks} from 'src/core/AppUtils';
import {AppConfig, stringifiedEndPoints, getURL} from 'src/config/AppConfig';
import rdflib from 'src/core/rdflib/rdflib';

function LinkCreationController($scope, $http) {
    console.log("LinkCreationController called ");
    var self = this;

    // Variables used by both whiteframes
    $scope.endpoints = AppConfig.endpoints;
    $scope.inputs = stringifiedEndPoints ($scope.endpoints);

    // Source
    $scope.sourcequeryresult;
    $scope.sourcelinks = [];
    $scope.sourceisLoading = false;
    $scope.sourceselected = [];

    $scope.sourceentryValueToDisplay = function entryValueToDisplay (entry) {
        return entry[$scope.sourcepropertyToDisplay];
    }

    $scope.sourcetriggerSearch = function triggerSearch (ev) {
        if (!$scope.sourceselectedInput) {
            alert ("A provider must be selected");
            return;
        }
        var provider = $scope.sourceselectedInput.split(' / ');

        $scope.sourceisLoading = true;
        $scope.sourcepropertyToDisplay = "title";

        var url = getURL($scope.endpoints, provider, 'endpoint');
        // TODO: add $scope.queryfilter as a ?where parameter
        getResource($http, url, function (response) {
            $scope.sourcequeryresult = response[AppConstants.memberKey];
            // console.log($scope.sourcequeryresult);
            $scope.sourceisLoading = false;

            // TODO: implement the retrieval of resource shapes
            // (but is there something wrong with the way they are serialized in RDF/XML?)
            // var shapeURL = getURL($scope.endpoints, provider, 'shape');
            // getResource($http, shapeURL, function (response) {
            //     console.log ("shape");
            //     console.log(response);
            // });

            // Note: old code that was used to get the list of link properties dynamically
            // var elem = $scope.sourcequeryresult[0];
            // console.log(elem);
            // for (var f in elem) {
            //     if (!f.endsWith(".type")) {
            //         var t = f + ".type";
            //         if (elem[t] !== 'Literal' && f !== 'type') {
            //             $scope.sourcelinks.push(f);
            //         }
            //     }
            // }
        });
    };

    // Target
    $scope.targetqueryresult;
    $scope.targetisLoading = false;
    $scope.targetselected = [];

    $scope.targetentryValueToDisplay = function entryValueToDisplay (entry) {
        return entry[$scope.targetpropertyToDisplay];
    }

    $scope.targettriggerSearch = function triggerSearch (ev) {
        if (!$scope.targetselectedInput) {
            alert ("A provider must be selected");
            return;
        }
        var provider = $scope.targetselectedInput.split(' / ');

        $scope.targetisLoading = true;
        $scope.targetpropertyToDisplay = "title";

        var url = getURL($scope.endpoints, provider, 'endpoint');
        // TODO: add $scope.queryfilter as a ?where parameter
        getResource($http, url, function (response) {
            $scope.targetqueryresult = response[AppConstants.memberKey];
            // console.log($scope.targetqueryresult);
            $scope.targetisLoading = false;
        });
    };


    // Checkboxes
    $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
        }
    };
    $scope.exists = function (item, list) {
        return list.indexOf(item) > -1;
    };


    // Link creation
    $scope.createLink = function createLink (ev) {
        if ($scope.sourceselected.length == 0) {
            alert("At least one source resource must be selected.");
            return;
        }
        if ($scope.targetselected.length == 0) {
            alert("At least one target resource must be selected.");
            return;
        }
        if ($scope.sourcelinkselected == null) {
            alert ("A link property must be set.")
            return;
        }

        // console.log("Link Creation - source:");
        // console.log($scope.sourceselected);
        // console.log("Link Creation - link property:");
        // console.log($scope.sourcelinkselected);
        // console.log("Link Creation - target:");
        // console.log($scope.targetselected);

        for (var s in $scope.sourceselected) {
            var sourceID = $scope.sourceselected[s].identifier;
            getResource($http, sourceID, function (response) {
                // rdflib.serialize(undefined, response.rdfgraph, undefined, 'application/rdf+xml', function(err, str){
                //     console.log("update before");
                //     console.log(str);
                // });
                updateRDFGraphWithLinks(sourceID, response.rdfgraph, $scope.sourcelinkselected, $scope.targetselected);
                // rdflib.serialize(undefined, response.rdfgraph, undefined, 'application/rdf+xml', function(err, str){
                //     console.log("update after");
                //     console.log(str);
                // });
                updateResource($http, sourceID, response.rdfgraph, function (response) {
                    console.log(response);
                });
            });
        }
    }

}

export default LinkCreationController;
