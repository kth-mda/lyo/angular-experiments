
import AutomationController from 'src/views/components/automation/AutomationController';

export default {
  name : 'automation',
  config : {
    bindings         : {  items: '<' },
    templateUrl      : 'src/views/components/automation/Automation.html',
    controller       : [ '$routeParams', '$resource', AutomationController ]
  }
};

