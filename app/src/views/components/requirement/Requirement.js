
import RequirementController from 'src/views/components/requirement/RequirementController';

export default {
  name : 'requirement',
  config : {
    bindings         : {  items: '<' },
    templateUrl      : 'src/views/components/requirement/Requirement.html',
    controller       : [ RequirementController ]
  }
};

