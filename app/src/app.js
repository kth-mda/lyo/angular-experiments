import angular from 'angular';

import 'angular-animate';
import 'angular-aria';
import 'angular-material';
import 'angular-route';
import 'angular-resource';

import AppController from 'src/AppController';
import Core from 'src/core/core.module';
import Views from 'src/views/Views';

export default angular.module('starter-app', [
        'ngMaterial',
        'ngRoute',
        'ngResource',
        Core.name,
        Views.name,
    ])
    .config(($mdIconProvider, $mdThemingProvider) => {
        // Register the user `avatar` icons
        $mdIconProvider
            .defaultIconSet("./assets/svg/avatars.svg", 128)
            .icon("menu", "./assets/svg/menu.svg", 24)
            .icon("search", "./assets/svg/search.svg", 24)
            .icon("dashboard", "./assets/svg/dashboard.svg", 24)
            .icon("automation", "./assets/svg/automation.svg", 24)
            .icon("settings", "./assets/svg/settings.svg", 24)
            .icon("quality", "./assets/svg/quality.svg", 24)
            .icon("close", "./assets/svg/close.svg", 24)
            .icon("expand", "./assets/svg/expand.svg", 24)
            .icon("filebox", "./assets/svg/filebox.svg", 24)
            .icon("link", "./assets/svg/link.svg", 24)

            .icon("share", "./assets/svg/share.svg", 24)
            .icon("google_plus", "./assets/svg/google_plus.svg", 24)
            .icon("hangouts", "./assets/svg/hangouts.svg", 24)
            .icon("twitter", "./assets/svg/twitter.svg", 24)
            .icon("phone", "./assets/svg/phone.svg", 24);

        $mdThemingProvider.theme('default')
            .primaryPalette('brown')
            .accentPalette('red');
    })
    .config(['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.when('/dashboard', {
                template: '<dashboard items="app.items" selectiondialog="app.showSelectionDialog(ev)"></dashboard>'
            }).when('/automation', {
                template: '<automation></automation>',
            }).when('/quality', {
                template: '<quality></quality>',
            }).when('/requirement', {
                template: '<requirement items="app.items"></requirement>',
            }).when('/linkcreation', {
                template: '<link-creation items="app.items"></link-creation>',
            }).when('/search', {
                template: '<search></search>',
            }). otherwise('/dashboard');
        }
    ])
    .controller('AppController', AppController);
