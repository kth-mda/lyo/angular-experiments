
// For some obscure reasons, I have issues getting access to AppConfig from other files if the line above is commented...
var globalVar = "test global var";

var AppConfig = {};

AppConfig.endpoints = [
    {
        category: "Extended Requirement",
        providers: [
            {
                title: "Ext Req Provider",
                url: "http://localhost:8080/BasicAdaptor/services/req-services/RSP",
                endpoints: [
                    { title: "requirements", url: "http://localhost:8080/BasicAdaptor/services/req-services/RSP/extendedRequirements/requirements", shape: "http://localhost:8080/BasicAdaptor/services/resourceShapes/extendedRequirement"},
                ]
            },
        ]
    },
    {
        category: "Basic Domain",
        providers: [
            {
                title: "Basic Provider",
                url: "http://localhost:8080/BasicAdaptor/services/bd-services/BDSP",
                endpoints: [
                    { title: "softwareproducts", url: "http://localhost:8080/BasicAdaptor/services/bd-services/BDSP/resources/softwareproducts", shape: "http://localhost:8080/BasicAdaptor/services/resourceShapes/softwareProduct"},
                    { title: "developers", url: "http://localhost:8080/BasicAdaptor/services/bd-services/BDSP/resources/developers", shape: "http://localhost:8080/BasicAdaptor/services/resourceShapes/developer"},
                ]
            },
        ]
    },
    {
        category: "Requirement",
        providers: [
            {
                title: "ProR ProR_Search_Spec",
                url: "https://aide.md.kth.se/pror/services/rm-services/ProR_Search_Spec.reqif",
                endpoints: [
                    { title: "requirements", url: "https://aide.md.kth.se/pror/services/rm-services/ProR_Search_Spec.reqif/resources/requirements" },
                    { title: "collections", url: "https://aide.md.kth.se/pror/services/rm-services/ProR_Search_Spec.reqif/resources/collections" },
                    { title: "versions", url: "https://aide.md.kth.se/pror/services/rm-services/ProR_Search_Spec.reqif/resources/versions"},
                ]
            },
            {
                title: "ProR model",
                url: "https://aide.md.kth.se/pror/services/rm-services/model.reqif",
                endpoints: [
                    { title: "requirements", url: "https://aide.md.kth.se/pror/services/rm-services/model.reqif/resources/requirements" },
                    { title: "collections", url: "https://aide.md.kth.se/pror/services/rm-services/model.reqif/resources/collections" },
                    { title: "versions", url: "https://aide.md.kth.se/pror/services/rm-services/model.reqif/resources/versions"},
                ]
            }
        ]
    }
];

// export default AppConfig;

function stringifiedEndPoints (endpoints) {
    var arr = new Array(endpoints.size);
    for (var i in endpoints) {
        for (var j in endpoints[i].providers) {
            for (var k in endpoints[i].providers[j].endpoints) {
                arr[i] = endpoints[i].category + ' / '
                    + endpoints[i].providers[j].title + ' / '
                    + endpoints[i].providers[j].endpoints[k].title;
            }
        }
    }
    return arr;
}

function getURL (endpoints, provider, urlType) {
    for (var i in endpoints) {
        if (!(endpoints[i].category === provider[0])) {
            continue;
        }
        for (var j in endpoints[i].providers) {
            if (!(endpoints[i].providers[j].title === provider[1])) {
                continue;
            }
            for (var k in endpoints[i].providers[j].endpoints) {
                if (endpoints[i].providers[j].endpoints[k].title === provider[2]) {
                    if (urlType === 'endpoint') {
                        return endpoints[i].providers[j].endpoints[k].url;
                    } else if (urlType === 'shape') {
                        return endpoints[i].providers[j].endpoints[k].shape;
                    }
                }

            }
        }
    }
}
