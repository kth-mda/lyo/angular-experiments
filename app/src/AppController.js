import SelectionDialogController from 'src/core/components/selectionDialog/SelectionDialogController';
// import AppConfig from 'src/config/AppConfig';

function AppController($mdSidenav, $scope, $location, $mdDialog) {
    console.log("AppController called");
    var self = this;

    self.items = [
        {
            name: "Dashboard",
            link: "#!/dashboard",
            icon: "dashboard",
        },
        {
            name: "Automation",
            link: "#!/automation",
            icon: "automation",
        },
        {
            name: "Quality",
            link: "#!/quality",
            icon: "quality",
        },
        {
            name: "Link Creation",
            link: "#!/linkcreation",
            icon: "link",
        },
        {
            name: "Search",
            link: "#!/search",
            icon: "search",
        },
        {
            name: "Settings",
            link: "",
            icon: "settings",
        }
    ];
    self.selected = null;
    self.toggleList   = toggleSideNav;
    self.showSelectionDialog = showSelectionDialog;
    self.options = AppConfig.endpoints;
    // $scope.customFullscreen = true;

    // Load all registered users

    // UsersDataService
    //     .loadAllUsers()
    //     .then(function (users) {
    //         self.users = [].concat(users);
    //         self.selected = users[0];
    //     });

    // *********************************
    // Internal methods
    // *********************************

    function toggleSideNav() {
        $mdSidenav('left').toggle();
    }

    /**
     * Select the current avatars
     * @param menuId
     */
    // function selectUser(user) {
        // self.selected = angular.isNumber(user) ? $scope.users[user] : user;
    // }

    function selectView(item) {
        // console.log(kjsflkjsdf);
    }

    function showSelectionDialog(ev) {
        $mdDialog.show({
            // locals: {
            //     'endpoints': AppConfig.endpoints
            // },
            controllerAs: '$ctrl',
            controller: SelectionDialogController,
            bindToController: true,
            templateUrl: 'src/core/components/selectionDialog/SelectionDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: true
            // fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
            .then(function(answer) {
                $scope.selectedEntries = answer;
                console.log($scope.selectedEntries);
            }, function() {
                $scope.selectedEntries = 'Cancel';
            });
    };

}

export default ['$mdSidenav', '$scope', '$location', '$mdDialog', AppController];

